interface TokensModelInterface {
    id?: number,
    tokens: string,
    user_agent: string
}

class TokensModel {
    
    id: number | undefined;
    tokens: string | undefined;
    user_agent: string | undefined;

    constructor(data?: TokensModelInterface) {
        if (data) {
            this.id = data.id;
            this.tokens = data.tokens;
            this.user_agent = data.user_agent;
        }
    }

}

export default TokensModel;