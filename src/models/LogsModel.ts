interface LogsModelInterface {
    temperature: string,
    humidity: string,
    moisture: string,
    heat_index: string,
    exhaust_fan: string,
    mist_pump: string,
    heater: string,
    log_date: string,
    log_time: string,
    user_agent: string
}

class LogsModel {

    temperature: string | undefined;
    humidity: string | undefined;
    moisture: string | undefined;
    heat_index: string | undefined;
    exhaust_fan: string | undefined;
    mist_pump: string | undefined;
    heater: string | undefined;
    log_date: string | undefined;
    log_time: string | undefined;
    user_agent: string | undefined;

    constructor(data?: LogsModelInterface) {
        if (data) {
            this.temperature = data.temperature;
            this.humidity = data.humidity;
            this.moisture = data.moisture;
            this.heat_index = data.heat_index;
            this.exhaust_fan = data.exhaust_fan;
            this.mist_pump = data.mist_pump;
            this.heater = data.heater;
            this.log_date = data.log_date;
            this.log_time = data.log_time;
            this.user_agent = data.user_agent;
        }
    }

}

export default LogsModel;