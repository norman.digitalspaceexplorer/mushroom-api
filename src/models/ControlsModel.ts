interface ControlsModelInterface {
    id: number;
    mist_pump: string,
    user_agent: string
}

class ControlsModel {

    id: number | undefined;
    mist_pump: string | undefined;
    user_agent: string | undefined;

    constructor(data?: ControlsModelInterface) {
        if (data) {
            this.id = data.id;
            this.mist_pump = data.mist_pump;
            this.user_agent = data.user_agent;
        }
    }

}

export default ControlsModel;