import Controller from "@controllers/Controller";
import service from "@services/TokensService";

const postTokenNotif = async (request: any, response: any) => {
    await Controller.handleRequest(request, response, service.postTokenNotif);
}

const putNotificationTokens = async (request: any, response: any) => {
    await Controller.handleRequest(request, response, service.putNotificationTokens);
}

const TokensController = {
    postTokenNotif,
    putNotificationTokens
};

export default TokensController;