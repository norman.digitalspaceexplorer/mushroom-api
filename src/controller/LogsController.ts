import Controller from "@controllers/Controller";
import service from "@services/LogsService";

const postActivityLogs = async (request: any, response: any) => {
    await Controller.handleRequest(request, response, service.postActivityLogs);
}

const getActivityLogs = async (request: any, response: any) => {
    await Controller.handleRequest(request, response, service.getActivityLogs);
}

const LogsController = {
    postActivityLogs,
    getActivityLogs
};

export default LogsController;

