import Controller from "@controllers/Controller";
import service from "@services/ControlsService";

const putControls = async (request: any, response: any) => {
    await Controller.handleRequest(request, response, service.putControls);
}

const postControls = async (request: any, response: any) => {
    await Controller.handleRequest(request, response, service.postControls);
}

const getControls = async (request: any, response: any) => {
    await Controller.handleRequest(request, response, service.getControls);
}

const ControlsController = {
    putControls,
    postControls,
    getControls
}

export default ControlsController;