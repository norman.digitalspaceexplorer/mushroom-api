import LogsController from "@controllers/LogsController";
import ControlsController from "@controllers/ControlsController";
import ReadingsController from "@controllers/ReadingsController";
import TokensController from "@controllers/TokensController";

const controllers = {
    LogsController,
    ControlsController,
    ReadingsController,
    TokensController
};

export default controllers;