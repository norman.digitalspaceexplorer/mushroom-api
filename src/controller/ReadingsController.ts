import Controller from "@controllers/Controller";
import service from "@services/ReadingsService";

const getSensorReadings = async (request: any, response: any) => {
    await Controller.handleRequest(request, response, service.getSensorReadings);
}

const ReadingsController = {
    getSensorReadings
};

export default ReadingsController;