import config from "./database.config";
import { Sequelize } from "sequelize";

const host = config.DB_HOST;
const user = config.DB_USER;
const name = config.DB_NAME;
const pass = config.DB_PASS;

const connect = () => {
    return new Sequelize(String(name), String(user), pass, {
        host,
        dialect: "mysql",
        ssl: false,
        logging: false,
        define: { freezeTableName: true, timestamps: false },
        pool: {
            max: 20,
            min: 0,
            acquire: 6000,
            idle: 1000
        }
    });
};

const Database = {
    connect
};

export default Database;