import { transports, createLogger, format } from "winston";

const timber = createLogger({
	level: "info",
	format: format.combine(format.timestamp(), format.json()),
	defaultMeta: { service: "user-service" },
	transports: [
		new transports.Console(),
		new transports.File({
			filename: "logs/error.log",
			level: "error",
		}),
		new transports.File({ filename: "logs/combined.log" }),
	],
});

if (process.env['NODE_ENV'] !== "production") {
	timber.add(new transports.Console({ format: format.simple() }));
}

export default timber;