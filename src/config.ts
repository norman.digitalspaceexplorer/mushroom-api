import path from "path";

interface ConfigInterface {
	ROOT_DIR: string;
	URL_PORT: number;
	URL_PATH: string;
	USER_AGENT: string;
	DEFAULT_CONTROL: string;
	PROJECT_DIR: string;
	OPENAPI_YAML: string;
	FILE_UPLOAD_PATH: string;
}

const config: ConfigInterface = {
	ROOT_DIR: __dirname,
	URL_PORT: Number(process.env.PORT) || 3001,
	URL_PATH: process.env['URL_PATH']?.toString() || "",
	USER_AGENT: process.env['USER_AGENT']?.toString() || "",
	DEFAULT_CONTROL: "OFF",
	PROJECT_DIR: __dirname,
	OPENAPI_YAML: "",
	FILE_UPLOAD_PATH: ""
};

config.OPENAPI_YAML = path.join(
	config.OPENAPI_YAML.substring(0, config.OPENAPI_YAML.lastIndexOf("/")),
	"api",
	"openapi.yml"
);

export default config;