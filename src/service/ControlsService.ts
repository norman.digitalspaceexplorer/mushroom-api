import ControlsSchema from "@schemas/ControlsSchema";
import ControlsModel from "@models/ControlsModel";
import ReadingsSchema from "@schemas/ReadingsSchema";
import ReadingsModel from "@models/ReadingsModel";
import Service from "@services/Service";
import config from "@app/config";

import { Op } from "sequelize";

const getControls = ({ user_agent }) =>
    new Promise(async (resolve, reject) => {
        try {
            if (user_agent !== undefined && user_agent === config.USER_AGENT) {               
                const controls = await ControlsSchema.findOne({
                    attributes: ["mist_pump", "user_agent"],
                    where: { user_agent: { [Op.eq]: user_agent } },
                    limit: 1
                }).then(controls => controls?.get({ plain: true }));
                if (controls !== undefined) {
                    resolve(Service.successResponse({
                        status: 200,
                        data: controls
                    }));
                } else {
                    reject(Service.rejectResponse({
                        status: 400,
                        message: "Bad Request, Unprocessable Entity"
                    }));
                }
                return;
            }
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Get Controls"
            }));
        } catch (error) {
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Get Controls"
            }));
        }
    });

const putControls = ({ temperature, humidity, moisture, heat_index,
    exhaust_fan, mist_pump, heater, user_agent }) =>
    new Promise(async (resolve, reject) => {
        try {
            if (user_agent !== undefined && user_agent === config.USER_AGENT) {
                const log_date = new Date().toLocaleDateString();
                const log_time = new Date().toLocaleTimeString();
                const _temperature = String(temperature).split('Â').join(' ')
                    .split('%20').join(' ');
                const _heat_index = String(heat_index).split('Â').join(' ')
                    .split('%20').join(' ');
                const readings = await ReadingsSchema.upsert(
                    new ReadingsModel({ id: 1,
                        temperature: _temperature, humidity, moisture, heat_index: _heat_index, 
                        exhaust_fan, mist_pump, heater, log_date, log_time, user_agent
                    })
                ).then(readings => readings);
                const controls = await ControlsSchema.findOne({
                    attributes: ["mist_pump", "user_agent"],
                    where: { user_agent: { [Op.eq]: user_agent } },
                    limit: 1
                }).then(controls => controls?.get({ plain: true }));
                if (controls !== undefined && readings !== undefined) {
                    resolve(Service.successResponse({
                        status: 200,
                        data: controls
                    }));
                } else {
                    reject(Service.rejectResponse({
                        status: 400,
                        message: "Bad Request, Unprocessable Entity"
                    }));
                }
                return;
            }
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Get Controls"
            }));
        } catch (error) {
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Get Controls"
            }));
        }
    });

const postControls = ({ mist_pump, user_agent }) => 
    new Promise(async (resolve, reject) => {
        try {
            if (user_agent !== undefined && user_agent === config.USER_AGENT) {
                const control = mist_pump ? mist_pump : config.DEFAULT_CONTROL;
                const service = await ControlsSchema.upsert(
                    new ControlsModel({ id: 1, mist_pump: control, user_agent })
                ).then((service) => service);
                if (service !== undefined) {
                    resolve(Service.successResponse({
                        status: 200,
                        message: "Changing Control Successful",
                        data: service[0]
                    }));
                } else {
                    reject(Service.rejectResponse({
                        status: 500,
                        message: "Internal Server Error, Unprocessable Entity"
                    }));
                }
                return;
            }
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Post Controls, Invalid User Agent"
            }));
        } catch (error) {
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Post Controls, Something went wrong."
            }));
        }
    });

const ControlsService = {
    getControls,
    putControls,
    postControls
}

export default ControlsService;