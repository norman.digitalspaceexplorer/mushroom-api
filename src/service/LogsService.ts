import LogsSchema from "@schemas/LogsSchema";
import LogsModel from "@models/LogsModel";
import Service from "@services/Service";
import config from "@app/config";

import { Op } from "sequelize";

const postActivityLogs = ({ temperature, humidity, moisture, heat_index,
    exhaust_fan, mist_pump, heater, user_agent }) =>
    new Promise(async (resolve, reject) => {
        try {
            if (user_agent !== undefined && user_agent === config.USER_AGENT) {
                const log_date = new Date().toLocaleDateString();
                const log_time = new Date().toLocaleTimeString();
                const logs = await LogsSchema.create(
                    new LogsModel({
                        temperature, humidity, moisture, heat_index, exhaust_fan, mist_pump, heater,
                        log_date, log_time, user_agent
                    })
                ).then((logs) => logs.get({ plain: true }));
                if (logs !== undefined) {
                    resolve(Service.successResponse({
                        status: 200,
                        message: "Data Added Successfully"
                    }));
                } else {
                    reject(Service.rejectResponse({
                        status: 500,
                        message: "Failed to Add Data Logs"
                    }));
                }
                return;
            }
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Add Data Logs"
            }));
        } catch (error) {
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Add Data Logs"
            }));
        }
    });

const getActivityLogs = ({ page, user_agent }) =>
    new Promise(async (resolve, reject) => {
        try {            
            if (user_agent !== undefined && user_agent === config.USER_AGENT && page !== undefined) {
                const offset = Number(page) <= 0 ? 0 : Number(page)-1;
                const activity_logs = await LogsSchema.findAll({
                    attributes: ["id", "temperature", "humidity", "moisture", "heat_index",
                        "exhaust_fan", "mist_pump", "heater", "log_date", "log_time", "user_agent"],
                    where: { user_agent: { [Op.eq]: user_agent } },
                    limit: 10,
                    order: [["id", "DESC"]],
                    offset: (10 * offset)
                }).then((logs) => logs.map((value) => value.get({ plain: true })));
                if (activity_logs !== undefined) {
                    activity_logs.forEach((logs) => {
                        if (logs["exhaust_fan"] !== "ON" || logs["exhaust_fan"] !== "OFF") {
                            if (logs["exhaust_fan"].includes("ON", 0)) {
                                logs["exhaust_fan"] = "ON";
                            } else if (logs["exhaust_fan"].includes("OFF", 0)) {
                                logs["exhaust_fan"] = "OFF";
                            } else {
                                logs["exhaust_fan"] = "OFF";
                            }
                        }
                        if (logs["mist_pump"] !== "ON" || logs["mist_pump"] !== "OFF") {
                            if (logs["mist_pump"].includes("ON", 0)) {
                                logs["mist_pump"] = "ON";
                            } else if (logs["mist_pump"].includes("OFF", 0)) {
                                logs["mist_pump"] = "OFF";
                            } else {
                                logs["mist_pump"] = "OFF";
                            }
                        }
                    })
                    resolve(Service.successResponse({
                        status: 200,
                        message: "Data Logs Successfully Fetched",
                        data: activity_logs
                    }))
                } else {
                    reject(Service.rejectResponse({
                        status: 500,
                        message: "Failed to Get Data Logs, Invalid User Agent"
                    }));
                }
                return;
            }
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Get Data Logs"
            }));
        } catch (error) {
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Get Data Logs"
            }));
        }
    });

const LogsService = {
    postActivityLogs,
    getActivityLogs
};

export default LogsService;