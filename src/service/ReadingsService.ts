import ReadingsSchema from "@schemas/ReadingsSchema";
import Service from "@services/Service";
import config from "@app/config";

import { Op } from "sequelize";

const getSensorReadings = ({ user_agent }) =>
    new Promise(async (resolve, reject) => {
        try {
            if (user_agent !== undefined && user_agent === config.USER_AGENT) {
                const readings = await ReadingsSchema.findAll({
                    attributes: ["id", "temperature", "humidity", "moisture", "heat_index",
                    "exhaust_fan", "mist_pump", "heater", "log_date", "log_time", "user_agent"],
                    where: { user_agent: { [Op.eq]: user_agent } },
                    order: [["id", "DESC"]],
                    limit: 1
                }).then((readings) => readings.map((value) => value.get({ plain: true })));
                if (readings !== undefined) {
                    resolve(Service.successResponse({
                        status: 200,
                        message: "Sensor Readings Fetched Successfully",
                        data: readings
                    }));
                } else {
                    reject(Service.rejectResponse({
                        status: 500,
                        message: "Internal Server Error, Unprocessable Entity"
                    }));
                }
                return;
            }
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Get Sensor Readings"
            }));
        } catch (error) {
            console.log(error);
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Get Sensor Readings"
            }));
        }
    });

const ReadingsService = {
    getSensorReadings
};

export default ReadingsService;