import config from "@app/config";
import TokensModel from "@models/TokensModel";
import TokensSchema from "@schemas/TokensSchema";
import fs from "fs";

import { Op } from "sequelize";

import Service from "@services/Service";

import admin from "firebase-admin";
import { firebase_sdk } from "../vars/endpoint";

let isInitialized = false;

const postTokenNotif = ({ user_agent, title, body }) => 
    new Promise(async (resolve, reject) => {
        try {
            if (user_agent !== undefined && user_agent === config.USER_AGENT) {
                const tokens = await TokensSchema.findAll({
                    attributes: ["id","tokens"],
                    where: { user_agent: { [Op.eq]: user_agent } },
                    order: [["id", "DESC"]],
                    limit: 20
                }).then((tokens) => tokens.map((values) => values.get({ plain: true })));

                if (isInitialized === false) {
                    const service_account = fs.readFileSync(firebase_sdk.file, "utf8");
                    admin.initializeApp({
                        credential: admin.credential.cert(JSON.parse(service_account))
                    });
                    isInitialized = true;
                }

                if (tokens !== undefined) {
                    
                    const valid_tokens = Array();
                    tokens.forEach((t) => {
                        const db_token = String(t["tokens"]);
                        valid_tokens.push(db_token);
                    });

                    const payload = { notification: { title, body }, tokens: valid_tokens }
                    let notification_response = Array();
                    await admin.messaging().sendMulticast(payload)
                        .then((response) => notification_response = JSON.parse(JSON.stringify(response)));

                    if (notification_response !== undefined) {
                        let token_index = 0;
                        notification_response["responses"].forEach(async (r: any) => {
                            if (r["success"] === false) {

                                const token_id = Number(tokens[token_index]["id"]);
                                await TokensSchema.destroy({
                                    where: { id: { [Op.eq]: token_id } }
                                }).then((deletion) => deletion);

                            }
                            token_index += 1;
                        });
                    }

                    resolve(Service.successResponse({
                        status: 200,
                        message: "Notifications Sent Successfully",
                        sent_tokens: tokens,
                        notification_response
                    }));
                    return;
                }
                reject(Service.rejectResponse({
                    status: 500,
                    message: "Failed to Deliver Notification Message"
                }));
            }
        } catch (error) {
            console.log(error);
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Deliver Notification Message"
            }));
        }
    });

const putNotificationTokens = ({ user_agent, token, token_id }) => 
    new Promise(async (resolve, reject) => {
        try {
            if (user_agent !== undefined && user_agent === config.USER_AGENT) {

                const token_db = await TokensSchema.findOne({
                    attributes: ["id", "tokens"],
                    where: { id: { [Op.eq]: Number(token_id) } },
                    limit: 1
                }).then((token) => token?.get({ plain: true }));

                if (token_db !== undefined) {
                    
                    const update_token = await TokensSchema.upsert(
                        new TokensModel({
                            id: token_id, tokens: token, user_agent
                        })
                    ).then((token) => token.map((values) => values));
                    resolve(Service.successResponse({
                        status: 200,
                        message: "Token Updated Successfully",
                        data: update_token[0]
                    }));

                } else {
                    
                    const create_token = await TokensSchema.create(
                        new TokensModel({
                            tokens: token, user_agent
                        })
                    ).then((token) => token.get({ plain: true }));
                    resolve(Service.successResponse({
                        status: 200,
                        message: "Token Added Successfully",
                        data: create_token
                    }));
                    
                }
                return;
            }
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Add Notification Token"
            }));
        } catch (error) {
            console.log(error);
            reject(Service.rejectResponse({
                status: 500,
                message: "Failed to Add Notification Token"
            }));
        }
    });

const TokensService = {
    postTokenNotif,
    putNotificationTokens
};

export default TokensService;