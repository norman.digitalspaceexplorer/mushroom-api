require("dotenv").config();

import 'module-alias/register';

import config from "@app/config";
import logger from "@app/logger";
import ExpressServer from '@app/server';

class Server {

    express_server: any = ExpressServer;
    port = Number(config.URL_PORT);
    openapi = String(config.OPENAPI_YAML);

    launch = async() => {
        return new Promise(async () => {
            try {
                this.express_server = new ExpressServer(this.port, this.openapi);
                await this.express_server.launch();
                logger.info(`Express Server Running On Port ${this.port} ...`);
            } catch (e) {
                logger.error(`Promise(index.ts): Error Launching Server ... ${e}`);
            }
        })
    }

}

new Server().launch().catch((e) => logger.error(`Server(index.ts): Error Launching Server ... ${e}`));