import fs from "fs";
import path from "path";
import http from "http";
import jsYaml from "js-yaml";
import express from "express";
import cors from "cors";
import swagger_ui from "swagger-ui-express";
import * as OpenApiValidator from "express-openapi-validator";

import { response_entry } from "@app/vars/endpoint";
import config from "@app/config";

class ExpressServer {

    app: any;
    schema: any;
    server: any;

    port: number;
    openapi_path: string;

    constructor(port: number, openapi_yaml: any) {
        this.port = port;
        this.openapi_path = openapi_yaml;
        this.app = express();
        this.schema = jsYaml.load(fs.readFileSync(openapi_yaml, 'utf8'));
        this.buildMainRoute();
    }

    buildMainRoute() {
        this.app.use(cors());
        this.app.use(express.json());
		this.app.use(express.urlencoded({ extended: false }));
        this.app.use('/api-doc', swagger_ui.serve, swagger_ui.setup(this.schema));
        this.app.get('/', (req: any, res: any) => {
            const response = fs.readFileSync(response_entry.file, "utf8");
            res.json(JSON.parse(response));
        });
        this.app.get('/openapi', (req: any, res: any) => {
            res.sendFile(path.join(__dirname, "../", this.openapi_path));
        });
    }

    launch() {
        this.app.use(
            OpenApiValidator.middleware({
                apiSpec: this.openapi_path,
                validateRequests: true,
                validateResponses: true,
                operationHandlers: path.join(__dirname),
                fileUploader: { dest: config.FILE_UPLOAD_PATH }
            })
        );
        this.app.use((err: any, req: any, res: any, next: any) => {
            res.status(err.status || 500).json({
                message: err.message,
                errors: err.errors,
            });
        });
        this.server = http.createServer(this.app).listen(this.port);
        console.log(`Express Server Running On Port ${this.port} ...`);
    }

}

export default ExpressServer;