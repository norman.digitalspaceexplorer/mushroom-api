import { Sequelize, Model, DataTypes } from "sequelize";
import db from "@connections/database";

class LogsSchema extends Model {}

(async () => {
    const sequelize: Sequelize = await db.connect();
    LogsSchema.init(
        {
            temperature: DataTypes.STRING,
            humidity: DataTypes.STRING,
            moisture: DataTypes.STRING,
            heat_index: DataTypes.STRING,
            exhaust_fan: DataTypes.STRING,
            mist_pump: DataTypes.STRING,
            heater: DataTypes.STRING,
            log_date: DataTypes.STRING,
            log_time: DataTypes.STRING,
            user_agent: DataTypes.STRING
        },
        {
            sequelize,
            modelName: "adm_logs",
            indexes: [{ unique: true, fields: ["id"] }]
        }
    );
    await sequelize.sync({ alter: false });
})();

export default LogsSchema;