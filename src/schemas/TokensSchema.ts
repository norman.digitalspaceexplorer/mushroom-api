import { Sequelize, Model, DataTypes } from "sequelize";
import db from "@connections/database";

class TokensSchema extends Model {}

(async () => {
    const sequelize: Sequelize = await db.connect();
    TokensSchema.init(
        {
            tokens: DataTypes.STRING,
            user_agent: DataTypes.STRING
        },
        {
            sequelize,
            modelName: "sdk_tokens",
            indexes: [{ unique: true, fields: ["id"] }]
        }
    );
    await sequelize.sync({ alter: false });
})();

export default TokensSchema;