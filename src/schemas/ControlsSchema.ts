import { Sequelize, Model, DataTypes } from "sequelize";
import db from "@connections/database";

class ControlsSchema extends Model {}

(async () => {
    const sequelize: Sequelize = await db.connect();
    ControlsSchema.init(
        {
            mist_pump: DataTypes.STRING,
            user_agent: DataTypes.STRING
        },
        {
            sequelize,
            modelName: "controls",
            indexes: [{ unique: true, fields: ["id"] }]
        }
    );
    await sequelize.sync({ alter: false });
})();

export default ControlsSchema;