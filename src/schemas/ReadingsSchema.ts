import { Sequelize, Model, DataTypes } from "sequelize";
import db from "@connections/database";

class ReadingsSchema extends Model {}

(async () => {
    const sequelize: Sequelize = await db.connect();
    ReadingsSchema.init(
        {
            temperature: DataTypes.STRING,
            humidity: DataTypes.STRING,
            moisture: DataTypes.STRING,
            heat_index: DataTypes.STRING,
            exhaust_fan: DataTypes.STRING,
            mist_pump: DataTypes.STRING,
            heater: DataTypes.STRING,
            log_date: DataTypes.STRING,
            log_time: DataTypes.STRING,
            user_agent: DataTypes.STRING
        },
        {
            sequelize,
            modelName: "snr_readings",
            indexes: [{ unique: true, fields: ["id"] }]
        }
    );
    await sequelize.sync({ alter: false });
})();

export default ReadingsSchema;