import path from "path";

export const response_entry = {
    file: path.join(__dirname, "../json/default.json"),
    encoding: "utf8"
}

export const firebase_sdk = {
    file: path.join(__dirname, "../json/vetistech-44aac-firebase-adminsdk.json"),
    encoding: "utf8"
}